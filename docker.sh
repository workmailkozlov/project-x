#!/bin/bash
apt-get install curl
apt-get install apt-transport-https curl ca-certificates curl software-properties-common -y ;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - ;
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"  ;
apt-get install docker-ce -y ;
docker --version

curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose ;
chmod +x /usr/local/bin/docker-compose ;
mkdir wordpress ;

echo " version: "3"
services:
  database:
    image: mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: wppassword
      MYSQL_DATABASE: wpdb
      MYSQL_USER: wpuser
      MYSQL_PASSWORD: wppassword
    volumes:
      - mysql:/var/lib/mysql

  wordpress:
    depends_on:
      - database
    image: wordpress:latest
    restart: always
    ports:
      - "80:80"
    environment:
      WORDPRESS_DB_HOST: database:3306
      WORDPRESS_DB_USER: wpuser
      WORDPRESS_DB_PASSWORD: wppassword
      WORDPRESS_DB_NAME: wpdb
    volumes:
      ["./:/var/www/html"]
volumes:
  mysql: {} " >> docker-compose.yaml ; 
  cd wordpress ;  docker-compose up -d 
 
